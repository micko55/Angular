export class AuthService {
  isAuth: boolean = false;
  authName!: string;

  signIn(name: string){
    // Simule l'attente entre l'envoi de la requête et la réception par le serveur (2s)
    return new Promise(
      ((resolve, reject) => {
        setTimeout(() => {
          this.isAuth = true;
          this.authName = name;
          resolve(true);
        }, 2000);
      })
    );
  }
  // TODO
  signOut(){
    this.authName = "";
    this.isAuth = false;
  }
}
