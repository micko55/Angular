import {Subject, Subscription} from "rxjs";
import {Injectable, OnInit} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AppareilService implements OnInit {
  public appareilSubject: Subject<any> = new Subject<any[]>();

  private appareils!: Array<any>;

  constructor(private httpClient: HttpClient) {
    this.getAppareilsFromServer();
  }

  ngOnInit() {
  }

  emitAppareilSubject(){
    if (this.appareils) {
      this.appareilSubject.next(this.appareils.slice());
    }
  }

  getAppareilById(id:number){
    return this.appareils.find(
      (appareilObject) => {
        return appareilObject.id === id;
      }
    );
  }

  switchOnAll(){
    for (const appareil of this.appareils) {
      appareil.appareilStatus = true;
    }
    this.emitAppareilSubject();
  }

  switchOffAll(){
    for (const appareil of this.appareils) {
      appareil.appareilStatus = false;
    }
    this.emitAppareilSubject();
  }

  toggleAll(){
    for (const appareil of this.appareils) {
      appareil.appareilStatus = !appareil.appareilStatus;
    }
    this.emitAppareilSubject();
  }

  switchOn(index: number){
    this.appareils[index].appareilStatus = true;
    this.emitAppareilSubject();
  }
  switchOff(index: number){
    this.appareils[index].appareilStatus = false;
    this.emitAppareilSubject();
  }

  addAppareil(name: string, status: boolean){
    const isEmpty = this.appareils ? this.appareils.length == 0 : true;
    const appareilObject = {
      id: isEmpty ? 1 : this.appareils[(this.appareils.length - 1)].id + 1,
      appareilName: name,
      appareilStatus: status
    };
    if (isEmpty) {
      this.appareils = [appareilObject];
    } else {
      this.appareils.push(appareilObject);
    }
    this.emitAppareilSubject();
  }

  removeAppareil(index: number) {
    this.appareils.splice(index,1);
    this.emitAppareilSubject();
  }

  saveAppareilsToServer() {
    this.httpClient
      .put('https://http-client-demo-1b8c9-default-rtdb.europe-west1.firebasedatabase.app/appareils.json', this.appareils)
      .subscribe({
        next: () => console.log("Enregistrement terminé !"),
        error: (e) => console.log('Erreur de sauvegarde ! ' + e)
      });
  }

  getAppareilsFromServer() {
    this.httpClient
      .get<any>('https://http-client-demo-1b8c9-default-rtdb.europe-west1.firebasedatabase.app/appareils.json')
      .subscribe({
        next: (response) => {
          // could simply be this.appareils = response, but for animation sake, gotta do this
          if (response) {
            let o = 0;
            while (!response[o]) {
              o++;
            }
            for(let i = 0; i < response.length; i++, o++) {
              if (!this.appareils && response[o]) {
                this.appareils = [response[o]];
              } else if (!this.appareils[i] && response[o]){
                this.appareils.push(response[o]);
              } else if (this.appareils[i] && response[o]) {
                this.appareils[i].appareilName = response[o].appareilName;
                this.appareils[i].appareilStatus = response[o].appareilStatus;
                this.appareils[i].id = response[o].id;
              }
            }
          }
          this.emitAppareilSubject();
        },
        error: (e) => console.log("Erreur de chargement !" + e)
      });
  }
}
