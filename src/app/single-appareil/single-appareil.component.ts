import { Component, OnInit } from '@angular/core';
import {AppareilService} from "../services/appareil.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-single-appareil',
  templateUrl: './single-appareil.component.html',
  styleUrls: ['./single-appareil.component.css']
})
export class SingleAppareilComponent implements OnInit {

  appareil: any | undefined;
  appareilName: string = "Appareils";
  appareilStatus!: boolean;

  constructor(private appareilService: AppareilService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params["id"];
    this.appareil = this.appareilService.getAppareilById(+id)
    if (this.appareil) {
      this.appareilName = this.appareil.appareilName;
      this.appareilStatus = this.appareil.appareilStatus;
    }
  }

  toggleStatus() {
    this.appareil.appareilStatus = !this.appareil.appareilStatus;
    this.appareilStatus = this.appareil.appareilStatus;
  }

  getStatus(){
    return this.appareilStatus;
  }

  getColor(){
    return this.getStatus() ? "green" : "red";
  }

}
