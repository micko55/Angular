import { Component, OnInit } from '@angular/core';
import { AppareilService } from "../services/appareil.service";
import {AuthService} from "../services/auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.css']
})
export class AppareilViewComponent implements OnInit {
  public isAuth: boolean = false;
  public lastUpdate: Promise<Date> = new Promise<Date>(
    (resolve, reject) => {
      const date = new Date();
      setTimeout(() => { resolve(date); }, 2000);
    }
  )
  public appareils!: Array<any>;
  public appareilSubscription!: Subscription;


  constructor(private appareilService: AppareilService,
              private authService: AuthService) {

    setTimeout(()=>{ this.isAuth=authService.isAuth; }, 1000);
  }

  ngOnInit() {
    this.appareilSubscription = this.appareilService.appareilSubject.subscribe(
      (appareils: any[]) => {
        this.appareils = appareils;
      }
    );
    this.appareilService.emitAppareilSubject();
  }

  onAllumer(){
    this.appareilService.switchOnAll();
  }

  onEteindre(){
    this.appareilService.switchOffAll();
  }

  onToggle(){
    this.appareilService.toggleAll();
  }

  onSave() {
    this.appareilService.saveAppareilsToServer();
  }

  onFetch() {
    this.appareilService.getAppareilsFromServer();
  }
}
