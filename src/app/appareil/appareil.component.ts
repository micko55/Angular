import {Component, Input, OnInit} from '@angular/core';
import {AppareilService} from "../services/appareil.service";

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.css']
})
export class AppareilComponent implements OnInit {

  @Input() appareilName!: string;
  @Input() appareilStatus: boolean = false;
  @Input() indexOfAppareil!: number;
  @Input() id!: number;

  constructor(private appareilService: AppareilService) {
  }

  ngOnInit(): void {
  }

  getStatus(){
    return this.appareilStatus;
  }

  setStatus(value: boolean){
    this.appareilStatus = value;
  }

  getColor() {
    return this.getStatus() ? "green" : "red";
  }

  onSwitchOn(){
    this.appareilService.switchOn(this.indexOfAppareil);
  }
  onSwitchOff(){
    this.appareilService.switchOff(this.indexOfAppareil);
  }
  onToggle(){
    this.getStatus() ? this.onSwitchOff() : this.onSwitchOn();
  }

  onRemove(index: number){
    this.appareilService.removeAppareil(index);
  }

}
