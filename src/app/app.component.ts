import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  secondes: number = 0;
  counterSubscription!: Subscription;

  constructor() {
  }
  ngOnInit() {
    const counter = interval(1000);
    this.counterSubscription = counter.subscribe({
      next: (value: number) => this.secondes = value,
      error: (e) => console.log(e),
      complete: () => console.info('complete')
    });
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }


}

