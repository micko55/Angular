import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  public authStatus: boolean = false;
  public authName!: string;

  /*public myform!: FormGroup;
  public firstName!: FormControl;*/

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.authStatus = this.authService.isAuth;
    /*this.firstName = new FormControl('', Validators.required);
    this.myform = new FormGroup({
      firstName: this.firstName,
    });*/
  }

  onSignIn(){
    //this.authName = evt;
    // if (this.myform.valid) {
    //   this.authName = this.myform.value.firstName;
      this.authService.signIn(this.authName).then(
        () => {
          this.authStatus = this.authService.isAuth;
          this.router.navigate(['appareils']);
        }
      )
    //   this.myform.reset();
    // }
  }

  onSignOut(){
    this.authService.signOut();
    this.authStatus = this.authService.isAuth;
  }
}
